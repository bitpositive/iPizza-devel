# -*- coding: utf-8-unix -*-

# @file         ipizza_ctrl.py
# @brief        iPizza control command part for Rapsberry Pi

# @author       Takuma Kawamura
# @see          Version: CPython 2.7
# @copyright    2018 (C) Takuma Kawamura

from nucl_master import *

# Singleton class
class iPizza_Ctrl:
    _instance = None
    
    def __new__( cls ):
        if cls._instance is None:
            cls._instance = super().__new__( cls )
        return cls._instance
    
    def __init__( self ):
        self.nuclPort = NuCL_Master( '/dev/serial0', 7, 115200 )

    # SWAP X n Y. Software Coor n Hardware Coor is reversed!
    def move( self, posY, posX, rpmY, rpmX ):
        packetBuffer = bytearray(7)
        
        packetBuffer[0] = 0        # Command MOVE
        packetBuffer[1] = ( posX >> 8 & 0xFF )
        packetBuffer[2] = posX & 0xFF
        packetBuffer[3] = posY >> 8 & 0xFF
        packetBuffer[4] = posY & 0xFF
        packetBuffer[5] = int( rpmX / 10 ) & 0xFF
        packetBuffer[6] = int( rpmY / 10 ) & 0xFF        
        
        self.nuclPort.send( packetBuffer )
        
    def rotate( self, deg ):
        deg = deg - 9           # 補整        
        deg_180 = deg % 180
        
        packetBuffer = bytearray(7)
        
        packetBuffer[0] = 1        # Command ROTATE
        packetBuffer[1] = int( deg_180 ) & 0xFF
        packetBuffer[2] = 0
        packetBuffer[3] = 0
        packetBuffer[4] = 0
        packetBuffer[5] = 0
        packetBuffer[6] = 0
        
        self.nuclPort.send( packetBuffer )
        
    def setCutter( self, isLower ):
        packetBuffer = bytearray(7)
        packetBuffer[0] = 2        # Command SET_CUTTER
        packetBuffer[1] = int(isLower) & 0x01
        
        packetBuffer[2] = 0
        packetBuffer[3] = 0
        packetBuffer[4] = 0
        packetBuffer[5] = 0
        packetBuffer[6] = 0
        
        self.nuclPort.send( packetBuffer )

    def finish( self ):
        packetBuffer = bytearray(7)
        packetBuffer[0] = 3        # Command FINISH
        packetBuffer[1] = 0
        packetBuffer[2] = 0
        packetBuffer[3] = 0
        packetBuffer[4] = 0
        packetBuffer[5] = 0
        packetBuffer[6] = 0
        
        self.nuclPort.send( packetBuffer )

