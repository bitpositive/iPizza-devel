# -*- coding:utf-8-unix -*-
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import
from future_builtins import *

import sys
import cv2
import numpy as np

def DetectPizza(cameraNum):     # check
    # カメラの初期化
    cap = cv2.VideoCapture(cameraNum)
    cap.set(cv2.cv.CV_CAP_PROP_FPS, 10)
    cap.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, 1200)
    cap.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, 900)
    
    # 青の範囲
    BlueLower = np.array([120, 50, 50]) # HSV check here
    BlueUpper = np.array([140, 255, 255])
    
    # 機械座標でのマーカー距離
    MCoorDistance = 875  # x0.4mm
    # ソフトウェア内でのマーカー距離
    SCoorDistance = 900
    
    # ソフトウェア座標から機械座標への変換係数
    CFStoM = MCoorDistance / float(SCoorDistance)

    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()
        
        # 正方形にトリミング
        frame = frame[0:900, 150:1050]
        frame = cv2.flip(frame, -1)
        
        #### マーカー認識フェーズ ####
        hsv = cv2.cvtColor(frame, cv2.cv.CV_BGR2HSV)
        h = hsv[:, :, 0]
        s = hsv[:, :, 1]
        mask = np.zeros(h.shape, dtype=np.uint8)
        mask = cv2.inRange(hsv, BlueLower, BlueUpper)
        contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        rects = []
        for contour in contours:
            # 輪郭の領域を計算
            area = cv2.contourArea(contour)
            print(area)
            # 小さすぎる領域と大きすぎる領域を除外
            if area < 100 or area > 800:
                continue
            # 外接矩形
            if len(contour) > 0:
                rect = cv2.boundingRect(contour) 
                cv2.rectangle(frame, (rect[0], rect[1]), (rect[0]+rect[2], rect[1]+rect[3]), (0, 0, 255), thickness=1)
                rects.append(rect)

        
        if len(rects) == 4:
            # 4点をソート
            sorted_rects = sorted(rects, key=lambda x:x[1]) # sort: y
            sorted_rects_top = sorted(rects[:2], key=lambda x:x[0])
            sorted_rexts_bottom = sorted(rects[2:], key=lambda x:x[0], reverse=True)
            sorted_rects = np.array(sorted_rects_top + sorted_rexts_bottom, dtype='float32')
            
            size = tuple(np.array([SCoorDistance, SCoorDistance]))
            perspective1 = np.float32([[sorted_rects[0][0] + sorted_rects[0][2] / 2.0, sorted_rects[0][1] + sorted_rects[0][3] / 2.0],
                                       [sorted_rects[1][0] + sorted_rects[1][2] / 2.0, sorted_rects[1][1] + sorted_rects[1][3] / 2.0],
                                       [sorted_rects[2][0] + sorted_rects[2][2] / 2.0, sorted_rects[2][1] + sorted_rects[2][3] / 2.0],
                                       [sorted_rects[3][0] + sorted_rects[3][2] / 2.0, sorted_rects[3][1] + sorted_rects[3][3] / 2.0]])
            perspective2 = np.float32([[0, SCoorDistance],
                                       [SCoorDistance, SCoorDistance],
                                       [SCoorDistance, 0],
                                       [0, 0]])
            # 透視変換行列を生成
            psp_matrix = cv2.getPerspectiveTransform(perspective1, perspective2)
            # 透視変換を行い、出力
            psp = cv2.warpPerspective(frame, psp_matrix, size)            
            
            #### ピザ認識フェーズ ####
            pizza_img = psp
            pizza_img = cv2.cvtColor(pizza_img,cv2.cv.CV_BGR2GRAY)
            pizza_img = cv2.GaussianBlur(pizza_img, (11, 11), 0)
            pizza_img = cv2.adaptiveThreshold(pizza_img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 25, 4)
            contours, hierarchy = cv2.findContours(pizza_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            # カラー化
            pizza_img = cv2.cvtColor(pizza_img, cv2.cv.CV_GRAY2RGB)
            
            pizza_contour = 0
            if len(contours) > 0:
                for i, contour in enumerate(contours):
                    area = cv2.contourArea(contour)
                    # 大きすぎる領域を除外
                    if area > 3000:
                        pizza_contour = contour
                        continue
                    
            if pizza_contour is 0:
                return 0
            else:
                # 最小外接円
                (x, y), radius = cv2.minEnclosingCircle(pizza_contour)
                sx = int(x)
                sy = int(y)
                center = (sx, sy)
                sradius = int(radius)

                cv2.circle(pizza_img, center, sradius, (255,255,0), thickness=2)
                cv2.circle(pizza_img, center, 4, (0,255,0), thickness=-1)
                
                mx = int(CFStoM * sx)
                my = int(CFStoM * sy)
                mradius = int(CFStoM * sradius)
                
                ### debug ###
                cv2.imshow('pizzaContours', pizza_img)
                while True:
                    k = cv2.waitKey(1) & 0xff
                    if k == ord('y'):#121:                 # Y key
                        cap.release()
                        cv2.waitKey(1)
                        cv2.destroyAllWindows()
                        cv2.waitKey(1)
                        cv2.waitKey(1)
                        cv2.waitKey(1)
                        cv2.waitKey(1)
                        cv2.waitKey(1)
                        cv2.waitKey(1)
                        return mx, my, mradius
                    elif k == ord('n'):#110:            # N key
                        cv2.waitKey(1)
                        cv2.destroyAllWindows()
                        cv2.waitKey(1)
                        cv2.waitKey(1)
                        cv2.waitKey(1)
                        cv2.waitKey(1)
                        cv2.waitKey(1)
                        cv2.waitKey(1)
                        break
        
        ### debug ###
        cv2.imshow('frame', frame)
        cv2.waitKey(1)

