# -*- coding:utf-8-unix -*-

import sys
import math

def sin(deg):
    return math.sin(math.radians(deg))

def cos(deg):
    return math.cos(math.radians(deg))

def tan(deg):
    return math.tan(math.radians(deg))

def asin(x):
    return math.degrees(math.asin(x))

def acos(x):
    return math.degrees(math.acos(x))

def atan(x):
    return math.degrees(math.atan(x))

def atan2(y, x):
    return math.degrees(math.atan2(y, x))

def degrees(rad):
    return math.degrees(rad)

def radians(deg):
    return math.radians(deg)
