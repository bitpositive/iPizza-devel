# -*- coding: utf-8-unix -*-

# @file         nucl_master.py
# @brief        Nerve uc Connect Layer Master Part for Rapsberry Pi

# @author       Takuma Kawamura
# @see          Version: CPython 2.7
# @copyright    2018 (C) Takuma Kawamura

import serial

class NuCL_Master:
    def __init__( self, devPath, payloadSize, baud ):
        self.port = serial.Serial( devPath, baud, timeout=10)
        self.payloadSize = payloadSize

    def __del__( self ):
        self.port.close()
        
    def send( self, payloadArray ):
        packetBuffer = bytearray()
        packetBuffer.append( ord( '@' ) & 0xFF )
        packetBuffer.append( self.payloadSize )
        packetBuffer.extend( payloadArray )
        checksum = 0
        for b in payloadArray:
            checksum = checksum + b
        packetBuffer.append( checksum & 0xFF )
        packetBuffer.append( ord( '\n' ) )

        self.port.write( packetBuffer )


