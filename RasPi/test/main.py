# -*- coding:utf-8-unix -*-

import pizzadetecttest as dp


result = dp.DetectPizza(1)

if result is 0:
    print("Pizza not found")
else:
    (x, y, r) = result
    print('MCoor X: {0}, Y: {1}, R: {2}'.format(x, y, r))
