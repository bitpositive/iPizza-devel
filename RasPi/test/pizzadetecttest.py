# -*- coding:utf-8-unix -*-

import sys
import cv2
import numpy as np

def DetectPizza(cameraNum):     
    # カメラの初期化
    cap = cv2.VideoCapture(cameraNum)
    # cap.set(cv2.CAP_PROP_FPS, 10)
    # cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1200)
    # cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 900)
    cap.set(cv2.cv.CV_CAP_PROP_FPS, 10)
    cap.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, 1200)
    cap.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, 900)
    
    # 青の範囲
    BlueLower = np.array([110, 50, 50])
    BlueUpper = np.array([130, 255, 255])
    
    # 機械座標でのマーカー距離
    MCoorDistance = 1000  # x0.4mm
    # ソフトウェア内でのマーカー距離
    SCoorDistance = 900
     
    # ソフトウェア座標から機械座標への変換係数
    CFStoM = 1000 / float(900)
     
    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()
        
        # 正方形にトリミング
        frame = frame[0:SCoorDistance, int(SCoorDistance / 6.0):int(SCoorDistance * 9 / 6.0)]
        
        #### マーカー認識フェーズ ####
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        h = hsv[:, :, 0]
        s = hsv[:, :, 1]
        mask = np.zeros(h.shape, dtype=np.uint8)
        mask = cv2.inRange(hsv, BlueLower, BlueUpper)
        image, contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        rects = []
        for contour in contours:
            # 輪郭の領域を計算
            area = cv2.contourArea(contour)
            # 小さすぎる領域と大きすぎる領域を除外
            if area < 1000 or area > 2000:
                continue
            # 外接矩形
            if len(contour) > 0:
                rect = cv2.boundingRect(contour)
                cv2.rectangle(frame, (rect[0], rect[1]), (rect[0]+rect[2], rect[1]+rect[3]), (0, 0, 255), thickness=1)
                rects.append(rect)

        
        if len(rects) == 4:
            # 4点をソート
            sorted_rects = sorted(rects, key=lambda x:x[1]) # sort: y
            sorted_rects_top = sorted(rects[:2], key=lambda x:x[0])
            sorted_rexts_bottom = sorted(rects[2:], key=lambda x:x[0], reverse=True)
            sorted_rects = np.array(sorted_rects_top + sorted_rexts_bottom, dtype='float32')
            
            size = tuple(np.array([SCoorDistance, SCoorDistance]))
            perspective1 = np.float32([[sorted_rects[0][0] + sorted_rects[0][2] / 2.0, sorted_rects[0][1] + sorted_rects[0][3] / 2.0],
                                       [sorted_rects[1][0] + sorted_rects[1][2] / 2.0, sorted_rects[1][1] + sorted_rects[1][3] / 2.0],
                                       [sorted_rects[2][0] + sorted_rects[2][2] / 2.0, sorted_rects[2][1] + sorted_rects[2][3] / 2.0],
                                       [sorted_rects[3][0] + sorted_rects[3][2] / 2.0, sorted_rects[3][1] + sorted_rects[3][3] / 2.0]])
            perspective2 = np.float32([[0, SCoorDistance],
                                       [SCoorDistance, SCoorDistance],
                                       [SCoorDistance, 0],
                                       [0, 0]])
            # 透視変換行列を生成
            psp_matrix = cv2.getPerspectiveTransform(perspective1, perspective2)
            # 透視変換を行い、出力
            psp = cv2.warpPerspective(frame, psp_matrix, size)
            
            
            #### ピザ認識フェーズ ####
            pizza_img = psp
            pizza_img = cv2.cvtColor(pizza_img,cv2.COLOR_BGR2GRAY)
            pizza_img = cv2.GaussianBlur(pizza_img, (11, 11), 0)
            pizza_img = cv2.adaptiveThreshold(pizza_img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 25, 4)
            pizza_img, contours, hierarchy = cv2.findContours(pizza_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            # カラー化
            pizza_img = cv2.cvtColor(pizza_img, cv2.COLOR_GRAY2RGB)
            
            pizza_contour = 0
            if len(contours) > 0:
                for i, contour in enumerate(contours):
                    area = cv2.contourArea(contour)
                    # 小さすぎる領域を除外
                    if area > 3000:
                        pizza_contour = contour
                        continue
                    
            if pizza_contour is 0:
                #print('pizza not found')
                return 0
            else:
                # 最小外接円
                (x, y), radius = cv2.minEnclosingCircle(pizza_contour)
                sx = int(x)
                sy = int(y)
                center = (sx, sy)
                sradius = int(radius)
                pizza_img = cv2.circle(pizza_img, center, sradius, (255,255,0), 2)
                pizza_img = cv2.circle(pizza_img, center, 4, (0,255,0), -1)
                
                mx = int(CFStoM * sx)
                my = int(CFStoM * sy)
                mradius = int(CFStoM * sradius)
                
                cv2.imshow('pizzaContours', pizza_img)
                while True:
                    k = cv2.waitKey(1)
                    if k == 121:                 # Y key
                        cap.release()
                        cv2.destroyAllWindows()
                        return mx, my, mradius
                    elif k == 110:            # N key
                        cv2.destroyAllWindows()
                        break
        
        ### debug ###
        cv2.imshow('frame', frame)
        cv2.waitKey(1)

