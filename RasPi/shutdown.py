#!/usr/bin/python

# -*- coding: utf-8-unix -*-
# Version: CPython 3

import time
import RPi.GPIO as GPIO
import os

pinnumber=36
GPIO.setmode(GPIO.BOARD)

GPIO.setup(pinnumber,GPIO.IN,pull_up_down=GPIO.PUD_UP)

while True:
    GPIO.wait_for_edge(pinnumber, GPIO.FALLING)
    sw_counter = 0

    while True:
        sw_status = GPIO.input(pinnumber)
        if sw_status == 0:
            sw_counter = sw_counter + 1
            if sw_counter >= 50:
                print("long press")
                os.system("sudo shutdown -h now")
                break
        else:
            print("short press")
            break

        time.sleep(0.01)

    print(sw_counter)
