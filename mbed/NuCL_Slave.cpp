// -*- coding: utf-8-unix -*-
/*!
  @file			NuCL_Slave.cpp
  @brief		Nerve-Microcontroller connect Layer Slave Part

  @author		T.Kawamura
  @version	1.1
  @date		2017-06-24	新規作成
  @date		2017-08-24	バグ修正
	@date		2017-09-29	パケット間タイムアウト追加
	
  @copyright	2017(C) T.Kawamura
*/

#include "NuCL_Slave.hpp"

/*!
  @brief Constructor of NuCL_Slave.
	
  @param rxpin Serial recieve pin.
  @param payload_size Size of payload.
  @param baudrate Serial baudrate. Default value is 115200(bps).
  @param uart_fifo_buffer_size Serial rx/tx FIFO buffer size(byte).
*/
NuCL_Slave::NuCL_Slave(PinName rxpin, uint32_t payload_size, uint32_t baudrate, uint32_t uart_fifo_buffer_size){
	
	serial_port = new AsyncSerial(NC, rxpin, baudrate, uart_fifo_buffer_size);
	//pc = new AsyncSerial(USBTX, USBRX, 115200, 256);
	//pc->putc(0xEE);
	
	// Debug
	//led = new DigitalOut(LED2);
	
	time_out_timer = new Timer;
	time_out_timer->stop();
	time_out_timer->reset();
	
	load_status = STATE_WAIT_HEADER;
	
	size_of_payload = payload_size;
		
	payload_data_count = 0;
	
	return;
}

/*!
  @brief Destructor of NuCL_Slave.
	
  @param Nothing.
  @return Nothing.
*/
NuCL_Slave::~NuCL_Slave(void){
	delete serial_port;
	//delete led;
  delete time_out_timer;
	//delete pc;
	return;
}

/*!
  @brief Update NuCL Status.
  CALL THIS METHOD IN EVERY INFINITE LOOP!!

  @param Nothing.
  @retval NOT_READY_FOR_GET_PAYLOAD Not ready for getting payload.
  @retval READY_FOR_GET_PAYLOAD Ready for getting payload.
*/
update_result_e NuCL_Slave::update(void){
	volatile uint8_t data;
	uint32_t i;
	uint8_t checksum = 0;
	
	if( serial_port->readable() > 0 ) {
		
		if( time_out_timer->read_ms() > NUCL_TIMEOUT && load_status != STATE_WAIT_HEADER ){
			load_status = STATE_WAIT_HEADER;
		}
		time_out_timer->stop();
		time_out_timer->reset();
		
		data = serial_port->getc();
		
		//pc->putc(data);
		
		switch(load_status) {
		case STATE_WAIT_HEADER:
			if( data == NUCL_HEADER ) {	// '@'
				load_status = STATE_WAIT_LENGTH;    
			}
			break;

		case STATE_WAIT_LENGTH:
			if( data == size_of_payload ) {
				load_status = STATE_WAIT_PAYLOAD;
			} else {
				load_status = STATE_WAIT_HEADER;
			}
			break;

		case STATE_WAIT_PAYLOAD:
			if( payload_data_count < size_of_payload - 1 ) {
				packet_buffer[payload_data_count] = data;
				payload_data_count++;
			} else {
				packet_buffer[payload_data_count] = data;
				payload_data_count = 0;
				load_status = STATE_WAIT_CHECKSUM;
			}
			break;

		case STATE_WAIT_CHECKSUM:
			for( i = 0; i < size_of_payload; i++ ) {
				checksum += packet_buffer[i];
			}
                
			if( data == checksum ) {
				load_status = STATE_WAIT_FOOTER;
			} else {
				load_status = STATE_WAIT_HEADER;
			}
			break;
				
		case STATE_WAIT_FOOTER:
			if( data == NUCL_FOOTER ) {		// LF
				for( i = 0; i < size_of_payload; i++ ) {
					recieved_payload[i] = packet_buffer[i];
				}
				load_status = STATE_WAIT_HEADER;
					
				return READY_FOR_GET_PAYLOAD;
			} else {
				load_status = STATE_WAIT_HEADER;
			}
			
			break;
		}
	}
	
	time_out_timer->start();
	return NOT_READY_FOR_GET_PAYLOAD;
}

/*!
  @brief Get recieved payload by 1byte.
  PLEASE CALL THIS AFTER CHECK update() METHOD RESULT for "READY_FOR_GET_PAYLOAD" !!

  @param Payload index (Like array index).
  @return A part of recieved payload.
*/
uint8_t NuCL_Slave::get_payload_by_1byte(uint8_t index)
{
	return recieved_payload[index];
}

