// -*- coding:utf-8-unix -*-
/*!
  @file   main.cpp
  @author  <takuma@VAIO-FIT-15E>
  @date   Tue Jun 26 11:00:14 2018
  
  @brief  iPizza mbed main

  @target Nucleo F303K8
  
  @copyright  2018 (C) T.Kawamura
*/

///////////////////////////////////////////////////////////////////////////////
//                                Header Files                               //
///////////////////////////////////////////////////////////////////////////////
#include "mbed.h"   // Rev.147 Recommended
#include "AsyncSerial.hpp"
#include "AsyncStepper.hpp"
#include "NuCL_Slave.hpp"
#include "FIFO.hpp"
///////////////////////////////////////////////////////////////////////////////
//                                 Definitions                               //
///////////////////////////////////////////////////////////////////////////////
#define STEPS_PER_RESOLUTION 10         // x0.4mm
#define MCoorDistance 875              // x0.4mm

enum commands_e {
  MOVE, ROTATE, SET_CUTTER, FINISH
};
///////////////////////////////////////////////////////////////////////////////
//                            Function Prototypes                            //
///////////////////////////////////////////////////////////////////////////////
void InitRotate();
void InitX();
void InitY();
void GetCtrlData();
///////////////////////////////////////////////////////////////////////////////
//                               Grobal Objects                              //
///////////////////////////////////////////////////////////////////////////////
NuCL_Slave RxSerialPort( PB_7, 7, 115200, 256 );
AsyncStepper XStepper( PA_7, PA_6, PA_0, 250, FULL, NC, NC, NC, 200, FREE );
AsyncStepper YStepper( PA_4, PA_3, PA_1, 250, FULL, NC, NC, NC, 200, FREE );
PwmOut Servo1( PA_12 );
DigitalOut SolenoidValve1( PB_4 );
DigitalIn XInitButton( PA_10 );
DigitalIn YInitButton( PA_9 );
Ticker InterruptButtonCheck;

FIFO<uint8_t> ComQueue(150);
FIFO<uint32_t> XQueue(120);
FIFO<uint32_t> YQueue(120);
FIFO<uint8_t> XRPMDiv10Queue(120);
FIFO<uint8_t> YRPMDiv10Queue(120);
FIFO<uint8_t> SVQueue(80);
FIFO<uint8_t> DegQueue(80);

DigitalOut led(LED1);
AsyncSerial pc(USBTX, USBRX, 115200, 256);

bool IsXButtonPressed = false;
bool IsYButtonPressed = false;
bool WasXButtonPressed = false;
bool WasYButtonPressed = false;
    
bool isCutterLowered = false;
uint32_t latestPosX = 0;
uint32_t latestPosY = 0;  
///////////////////////////////////////////////////////////////////////////////
//                               Main Function                               //
///////////////////////////////////////////////////////////////////////////////
int main()
{
  pc.printf("\nHello\n");
  
  InterruptButtonCheck.attach_us( &InitRotate, 5000 );
  XInitButton.mode( PullUp );
  YInitButton.mode( PullUp );
  
  XStepper.Disable();
  YStepper.Disable();
  Servo1.period_ms( 20 );
  Servo1.pulsewidth_us( 500 );
  SolenoidValve1.write( 0 );
  
  ComQueue.clear();
  XQueue.clear();
  YQueue.clear();
  XRPMDiv10Queue.clear();
  YRPMDiv10Queue.clear();
  SVQueue.clear();
  DegQueue.clear();
  
  led = 0;
  
  while( 1 ) {    
    InitX();
    InitY();
    
    GetCtrlData();
    
    // iPizza Ctrl
    if ( XStepper.IsStopping() && YStepper.IsStopping() ) {
      if ( ComQueue.available() > 0 ) {
        uint8_t com = ComQueue.get();
        
        pc.printf( "\n\nComQueue count: %d\n", ComQueue.available() );
        
        uint32_t posX;
        uint32_t posY;
        uint32_t rpmX;
        uint32_t rpmY;
        uint32_t dirX = 0;
        uint32_t dirY = 0;
        uint32_t dXAbs = 0;   // * 0.4mm
        uint32_t dYAbs = 0;   // * 0.4mm
        uint8_t angle180 = 0;
        uint8_t lower = 0;
        
        switch (com) {
        case MOVE:
          posX = XQueue.get();
          posY = YQueue.get();
          rpmX = XRPMDiv10Queue.get() * 10;
          rpmY = YRPMDiv10Queue.get() * 10;
          
          if ( posX > MCoorDistance ) {
            posX = MCoorDistance;
          }
          if ( posY > MCoorDistance ) {
            posY = MCoorDistance;
          }
          
          if ( posX > latestPosX ) {
            dirX = 1;
            dXAbs = posX - latestPosX;
          } else {
            dirX = 0;
            dXAbs = latestPosX - posX;
          }
          if ( posY > latestPosY ) {
            dirY = 1;
            dYAbs = posY - latestPosY;
          } else {
            dirY = 0;
            dYAbs = latestPosY - posY;
          }
          
          pc.printf("\nX: %d, Y: %d, RPM: x%d y%d\n", posX, posY, rpmX, rpmY);
          pc.printf("\nlX: %d, lY: %d\n", latestPosX, latestPosY); 
          pc.printf("dirX: %d\n", dirX);
          pc.printf("dirY: %d\n", dirY);
          pc.printf("dX: %d\n", dXAbs);
          pc.printf("dY: %d\n", dYAbs);
          pc.printf("stepsX: %d\n", dXAbs * STEPS_PER_RESOLUTION);
          pc.printf("stepsY: %d\n", dYAbs * STEPS_PER_RESOLUTION);
          pc.printf("rpmX: %d\n", rpmX);
          pc.printf("rpmY: %d\n", rpmY);
          
          XStepper.SetRPM( rpmX );
          YStepper.SetRPM( rpmY );
          XStepper.Rotate( static_cast<direction_e>( dirX ), dXAbs * STEPS_PER_RESOLUTION );
          YStepper.Rotate( static_cast<direction_e>( dirY ), dYAbs * STEPS_PER_RESOLUTION );
          
          latestPosX = posX;
          latestPosY = posY;
          
          break;
          
        case ROTATE:
          if ( !isCutterLowered ) {
            Servo1.pulsewidth_us( 500 );
            wait(1.5);
            
            angle180 = DegQueue.get();
            Servo1.pulsewidth_us( angle180 * 190 / 18 + 500 );
            
            pc.printf("\nPulsewidth: %d, angle180: %d\n", angle180 * 190 / 18 + 500, angle180);
            
            //wait(2);
          }
          
          break;
          
        case SET_CUTTER:
          lower = SVQueue.get();          
          SolenoidValve1.write( lower );
          isCutterLowered = static_cast<bool>( lower );
          
          pc.printf("\nisLower: %d\n", lower);
          
          wait(1);
          
          break;
          
        case FINISH:
          ComQueue.clear();
          XQueue.clear();
          YQueue.clear();
          XRPMDiv10Queue.clear();
          YRPMDiv10Queue.clear();
          SVQueue.clear();
          DegQueue.clear();
          
          pc.printf("Finished.\n");
          
          break;
          
        default:
          break;
        }
      }
    }
  }
}


void InitX()
{
    if ( IsXButtonPressed && !WasXButtonPressed ) {
      XStepper.SetRPM( 200 );
      XStepper.Rotate( NEGATIVE, 300 );
      
      pc.printf("X button pressed\n");
    }
    WasXButtonPressed = IsXButtonPressed;
    
    return;
}

void InitY()
{
    if ( IsYButtonPressed && !WasYButtonPressed ) {
      YStepper.SetRPM( 200 );
      YStepper.Rotate( NEGATIVE, 300 );

      pc.printf("Y button pressed\n");
    }
    WasYButtonPressed = IsYButtonPressed;
    
    return;
}

void InitRotate()
{
  static uint32_t xlowcount = 0;
  static uint32_t ylowcount = 0;
  static uint32_t xhighcount = 0;
  static uint32_t yhighcount = 0;
    
  if ( XInitButton.read() == 0 ) {
    if ( ++xlowcount > 10 ) {
      xlowcount = 0;
      IsXButtonPressed = true;
    }
  }
    
  if ( XInitButton.read() == 1 ) {
    if ( ++xhighcount > 10 ) {
      xhighcount = 0;
      IsXButtonPressed = false;
    }
  }

  if ( YInitButton.read() == 0 ) {
    if ( ++ylowcount > 10 ) {
      ylowcount = 0;
      IsYButtonPressed = true;
    }
  }
    
  if ( YInitButton.read() == 1 ) {
    if ( ++yhighcount > 10 ) {
      yhighcount = 0;
      IsYButtonPressed = false;
    }
  }
  
  return;
}

void GetCtrlData()
{
  if ( RxSerialPort.update() == READY_FOR_GET_PAYLOAD ) {
    led = 1;
    pc.printf("Ready\n");
        
    uint8_t com = static_cast<uint8_t>( RxSerialPort.get_payload_by_1byte( 0 ) );
    ComQueue.put( com );
    
    switch ( com ) {
    case MOVE:
      XQueue.put( static_cast<uint32_t>( ( RxSerialPort.get_payload_by_1byte( 1 ) << 8 ) | RxSerialPort.get_payload_by_1byte( 2 ) ) );
      YQueue.put( static_cast<uint32_t>( ( RxSerialPort.get_payload_by_1byte( 3 ) << 8 ) | RxSerialPort.get_payload_by_1byte( 4 ) ) );
      XRPMDiv10Queue.put( static_cast<uint8_t>( RxSerialPort.get_payload_by_1byte( 5 ) ) );      
      YRPMDiv10Queue.put( static_cast<uint8_t>( RxSerialPort.get_payload_by_1byte( 6 ) ) );      
      
      break;
      
    case ROTATE:
      DegQueue.put( static_cast<uint8_t>( RxSerialPort.get_payload_by_1byte( 1 ) ) );
      
      break;
    case SET_CUTTER:
      SVQueue.put( static_cast<uint8_t>( RxSerialPort.get_payload_by_1byte( 1 ) ) );
      
      break;
    default:
      break;
    }
  }
}
